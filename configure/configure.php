<?php
function configure($page, $type) {
	
	switch($type) {
		
		case "general": {
			$page->template = "template_general.php";
			$page->style = array("style.css");
		} break;
		
		case "auth": {
			$page->template = "template_auth.php";
			$page->style =array("auth.css");
		} break;
		
		case "admin": {
			$page->template = "template_admin.php";
			$page->style = array("style.css");

			
		} break;
		
	}

	
	$page->menu = array (
		"Главная" => "index.php",
		"Ссылки" => "links.php",
		"Заметки" => "notes.php",
	);

	if ($page->user_id) {
		
		$page->menu["Файлы"] = "files.php";
		
		switch($page->user_type){
			
			case 0: {
				$page->menu["Администрирование"] = "admin.php";
				
				array_push($page->style, "type_admin.css");
				
				if ($type == "admin") {
					
					array_push($page->style, "admin.css");
					
					$page->menu_vertical = array (
						"Пользователи" => "users.php"
					);
					
				}
			}
			break;
			
			case 1: 
			break;
			
			case 2: {
				
				
			}
			break;
			
			default : {
				
			}
			break;
			
		}
		
		$page->menu["Выход"] = "exit.php";
		
	} else {
		
		$page->menu["Вход"] = "auth.php";
		
		$name = explode("/", $_SERVER["PHP_SELF"]);
		$name = $name[(count($name) -1)];

		if($name !="auth.php"){
			array_push($page->js, "auth.js");
		} else {
			array_push($page->js, "autha.js");
		}
		
		
	}
}
?>