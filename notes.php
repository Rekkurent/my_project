﻿<?php
	
	require_once("class/page_auth_class.php");
	require_once("class/notes_class.php");
	require_once("configure/configure.php");
	
	$page = new PageAuth();
	
	$type = "general";
	configure($page, $type);
	
	$page->title = "Заметки";

	if ($page->user_id) {
		
		$page->js = ["notes.js"];
		array_push($page->style, "notes.css");
		
		$note = new Notes($page->db, $page->user_id);
		$page->main .= $note->addNoteDisplay();
		$str= "";
		$note->notesDisplay($str);
		$page->main .= $str;

	} 
	
	$page->Display();	

	
	
	
?>