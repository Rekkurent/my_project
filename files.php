<?php

	require_once("class/page_auth_class.php");
	require_once("configure/configure.php");
	require_once("class/files_class.php");
	
	$page = new PageAuth();
	if ($page->user_id) {
		
		$type = "general";
		configure($page, $type);
		
		$page->js = ["files.js"];
		array_push($page->style, "files.css");
		
		$page->title = "Файлы";
		
		$files = new Files();
		$page->getUserName();
		
		$str = "<div id=\"current_dir\"><p>Каталог: </p><ul>";
		$str .= $files->showDir($_SERVER["DOCUMENT_ROOT"]."/../files/".$page->user_id);
		$str .= "</div></ul>";
		$page->main.= $str;
		
		/*
		$files->path = $page->login;
		$page->main.= $files->filesInfo();
		
		$page->main.= "<br>";
		$page->main.= $_SERVER["HTTP_HOST"];
		$page->main.= "<br>";
		$page->main.= $_SERVER["PHP_SELF"];
		$page->main.= "<br>";
		*/
		$page->main.= $files->uploadingDisplay();
		
		$page->Display();	
		
	} else {
		$page->redirect();
	}

?>