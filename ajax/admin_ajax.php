<?php

require_once("../class/db_class.php");
require_once("../class/admin_class.php");
require_once("../class/textModule.php");

session_start();

if (count($_SESSION) > 0) {
	
	
	$user_type = $_SESSION["user_type"];
	if ($user_type == 0) {
		
		if(isset($_POST["action"])) {
			$out = [];
			switch($_POST["action"]) {
				
				case "add_user" :  {
					
					$db = new database();
					$admin = new Admin($db);
					
					$admin->login=$_POST["add_login"];
					$admin->password=$_POST["add_password"];
					$admin->user_type=$_POST["user_type"];
					
					$admin->addUser();
					
					$text = new textModule();

					$user_type = $text->typeToText($admin->user_type);


					$out = [	
						"action" =>"add_user",
						"body" => [
							"id" =>$admin->user_id,
							"login" => $admin->login,
							"password" =>sha1($admin->password),
							"type" =>$user_type
						]
					];

					echo json_encode($out);
				}
				break;


				
				case "del_user" : {
					

					$db = new database();
					$admin = new Admin($db);
					$admin->user_id = $_POST["id"];
					$admin->deleteUser();

					$out = [	
						"action" =>"del_user",
						"body" => [
							"id" => $admin->user_id
						]
					];
					echo json_encode($out);
				}
				break;
	
				
			}
		}
		
	}
	


	
}



?>