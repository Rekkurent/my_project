<?php 

require_once("../class/page_auth_class.php");

$page = new PageAuth();

if ($page->user_id) {


	if(isset($_POST["action"])) {
		
		$out = [];
		
		switch($_POST["action"]) {
			
			case "auth" :  {
				
				$login = $_POST["login"];
				$password = $_POST["password"];

				$out = [	
					"action" =>"auth",
					"body" => [
						"login" => true,
						"password" => true
					]
				];
				
				echo json_encode($out);
			}
		}
	}
} else {
	$out = [	
	"action" =>"auth",
	"body" => [
		"login" => false,
		"password" => false
		]
	];
				
	echo json_encode($out);
}
?>