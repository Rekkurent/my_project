<?php
require_once("../class/notes_class.php");
require_once("../class/db_class.php");
require_once("../class/url_class.php");

session_start();

if (count($_SESSION) > 0) {
	
	$db = new database();
	$user_id = $_SESSION["user_id"];
	
	if(isset($_POST["action"])) {
		$out = [];
		switch($_POST["action"]) {
			
			case "add_note" :  {
				
				$title =$_POST["title"];
				$content =$_POST["note"];

				$note = new Notes($db, $user_id);
				$note_id = $note->addNote($title, $content);
				$out = [	
					"action" =>"add_note",
					"body" => [
						"id" => $note_id,
						"title" => $title,
						"note" =>$content
					]
				];
				
				echo json_encode($out);
			}
			break;
			case "display_notes" : {
				$str="";
				$note = new Notes($db, $user_id);
				$note->notesDisplay($str);
				echo $str;
			}
			break;
			case "del_all_notes" : {
				$str="";
				$note = new Notes($db, $user_id);
				$note->delAllNotes();
				$out = [	
					"action" =>"del_all_notes"
				];
				echo json_encode($out);
			}
			break;
			
			case "del_note" : {
				$id = $_POST["id"];
				$str="";
				$note = new Notes($db, $user_id);
				$note->delNote($id);
				$out = [	
					"action" =>"del_note",
					"body" => [
						"id" => $id
					]
				];
				echo json_encode($out);
			}
			break;
				case "update_note" : {
				$id = $_POST["id"];
				$title = $_POST["title"];
				$content = $_POST["note"];
				$content = json_decode($content, true);
				$content = objectToHtml($content);

				$str="";
				$note = new Notes($db, $user_id);
				$note->updateNote($id, $title, $content);
				$out = [	
					"action" =>"update_note",
					"body" => [
						"id" => $id,
						"title" => $title,
						"note" => $content
					]
				];
			
				
				echo json_encode($out);
			}
			break;
			
		}
	}

	
}

function objectToHtml($content) {
	$str="";
	
	$url_analyze = new Url();
	
	foreach($content as $key => $value) {
		switch($content[$key]["type"]){
			case "text": {
				
				$str.= $url_analyze->Show($content[$key]["text"]);
			} break;
			case "div": {
				$str.="<div>".$url_analyze->Show($content[$key]["text"])."</div>";
				//$str.=$content[$key]["text"]."\r\n";
			} break;
			case "span": {
				//$str.=$url_analyze->Show($content[$key]["text"]);
				$str.="<span>".$url_analyze->Show($content[$key]["text"])."</span>";
			} break;
			case "a": {
				
				if(isset($content[$key]["href"])){
					$href = "";
					$href="href=\"".$url_analyze->ShowURL($content[$key]["href"])."\"";
					$str.="<a ".$href.">".$content[$key]["text"]."</a>";
				}else {
					$str.=$url_analyze->Show($content[$key]["text"]);
				}
				
				
			} break;
			case "img": {
				
			} break;
		}
	}
	

	return $str;
	
}



?>