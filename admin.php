<?php

	require_once("class/page_admin_class.php");
	require_once("configure/configure.php");
		
	$page = new PageAdmin();

	if ($page->user_type === 0 ) {
		
		$type = "admin";
		configure($page, $type);
		
		$page->title = "Администрирование";

		$page->Display();	
	
	} else {
		
		$page->redirect();
		
	}
	
	
?>