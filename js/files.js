
window.addEventListener("load", loadHandler, false);

function loadHandler(){
	
	var button = document.getElementById("files_send");
	button.addEventListener("click", clickHandler, false);

	var arr = document.getElementsByClassName("file");
	for(var i = 0; i < arr.length; i++) {
		arr[i].addEventListener("mouseover", file_mouseover, false);
		arr[i].addEventListener("mouseout", file_mouseout, false);
		arr[i].addEventListener("click", file_click, false);
	}
}


function file_mouseover(event) {
	
	var arr = {
		"delete_file": "icons/delete_note_1.png",
		"edit_file": "icons/edit_note_1.png",
		"cancel_file": "icons/cancel_note_1.png",
		"save_file": "icons/save_note_1.png"
	};
	
	var key = event.target.className;
	
	if (key in arr) {
		event.target.src = arr[key];
	}
	
}

function file_mouseout(event) {
	
	var arr = {
		"delete_file": "icons/delete_note_0.png",
		"edit_file": "icons/edit_note_0.png",
		"cancel_file": "icons/cancel_note_0.png",
		"save_file": "icons/save_note_0.png"
	};
	
	var key = event.target.className;
	
	if (key in arr) {
		event.target.src = arr[key];
	}
	
}


function file_click(event) {
	
	if (event.target.className == "delete_file") {
		event.stopPropagation();
		event.preventDefault();
		
		var dynamicForm = new FormData();
		
		dynamicForm.append("action","del_file");
		dynamicForm.append("name", event.currentTarget.firstElementChild.innerText);
		dynamicForm.append("full_path", event.currentTarget.firstElementChild.href);
		postForm(dynamicForm, xhrDelFileHandler);
		
	} 
	
}

function xhrDelFileHandler(e){

	if (this.responseText!="") {
		try {

			
			//var newstr = replaceAll("\r\n", "\\n", this.responseText);
			var obj = JSON.parse(this.responseText);
		
			if (obj!= undefined && obj["action"]){
				switch(obj["action"]) {
					case "del_file": {
						var del = document.getElementsByClassName("file");
						for(var i = 0; i < del.length; i++){
							if(del[i].firstElementChild.innerText == obj["body"]["name"]) {
								var parent = del[i].parentElement;
								parent.removeChild(del[i]);
								break;
							}
						}

					}
					break;
					
				}
			}
		} catch (err) {
			var el = document.createTextNode(this.responseText);
			output.appendChild(el);
		}

	}
	
}


function clickHandler(e){ 
	
	e.stopPropagation();
	e.preventDefault();
	
	var dynamicForm = new FormData();
	
	var files_1 = document.getElementById("files_ajax");
	var f1_length = files_1.files.length;


	for(var i = 0; i < f1_length; i++) { 
		dynamicForm.append(files_1.name+"_"+i, files_1.files[i]);
	}
	
	if (f1_length > 0) {
		dynamicForm.append("action","upload_files");
		postForm(dynamicForm, xhrLoadHandler);
		
	}
	

	
}

function xhrLoadHandler(e){

	var parent = document.getElementById("input");
	var child = parent.firstElementChild;
	var new_child = child.cloneNode(false);
	
	parent.replaceChild(new_child, child);


	var output  = document.getElementById("current_dir");
	output.innerHTML = this.responseText;
	
	var arr = document.getElementsByClassName("file");
	for(var i = 0; i < arr.length; i++) {
		arr[i].addEventListener("mouseover", file_mouseover, false);
		arr[i].addEventListener("mouseout", file_mouseout, false);
		arr[i].addEventListener("click", file_click, false);
	}
}

function postForm(form, handler){
	
	var xhr = new XMLHttpRequest();
	xhr.open("POST", "ajax/files_ajax.php");
	xhr.addEventListener("load", handler, false);
	xhr.send(form);
	
}
