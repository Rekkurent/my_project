<!DOCTYPE html>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<?php 
			$this->titleDisplay(); 
			$this->styleDisplay(); 
			$this->jsDisplay();
		?>
	</head>
	<body>
		<div class="header">
			<h1>The Notebook</h1>
		</div>
		<?php 
			$this->menuDisplay($this->menu, "horizontal_menu", "admin.php"); 
		?>
		<div class="main">	
			<?php 
				$this->menuDisplay($this->menu_vertical, "vertical_menu", $this->current_page); 
			?>
			<div class="content">
			<?php 
				$this->mainDisplay(); 
			?>
			</div>
			<div class="footer"></div>
		</div>
	</body>
</html>

