
<?php

	require_once("page_auth_class.php");
	class PageAdmin extends PageAuth{

		
		public function usersListDisplay() {
			$this->db->connectToDatabase();
			$this->db->query = "select user_id, login, password, user_type from users";
			$this->db->queryExecution();
			
			$descrText = ["Логин", "Пароль", "Уровень доступа",""];
			
			$result="<form name=\"view_user\"  id=\"form_view\" ><table>";
			
			$result .= "<caption>Пользователи</caption>";
			$result .= "<tbody><tr>";
			
			foreach($descrText as $value) {
				$result .= "<th>".$value."</th>";
			}
			
			$result .= "</tr>";
			
			$obj = $this->db->arr_assoc;
			foreach($obj as $key => $value){

				$str ="<tr id=\"".$obj[$key]["user_id"]."\"class=\"user\">".
						"<td class=\"login_user\">".$obj[$key]["login"]."</td>".
						"<td class=\"password_user\">".$obj[$key]["password"]."</td>".
						"<td class=\"type_user\">".$this->text_module->typeToText($obj[$key]["user_type"])."</td>".
						"<td class=\"menu\">".
						"<img src=\"icons/delete_note_0.png\" class=\"delete_user\">".
						"</td>".
						"</tr>";
				$result .=$str;
			}
			$result .="</tbody></table></form>";
	
			return $result;
		}
		
		public function printAddUser() { 


			$str  = "<form name=\"add_user\" id=\"form_add\"><table>";
			$str .= "<caption>Добавить пользователя</caption>";
			$str .= "<tr><td>Логин</td><td>Пароль</td><td>Уровень доступа</td></tr>";
			$str .= "<tr>";
			$str .= "<td><input type=\"text\" name=\"add_login\"></td>";
			$str .= "<td><input type=\"password\" name=\"add_password\"></td>";
			$str .= "<td><select name=\"user_type\">";
			$str .= "<option value=\"0\">Администратор</option>";
			$str .= "<option value=\"2\">Пользователь</option>";
			$str .= "</select></td></tr>";
			$str .= "<tr><td colspan=\"3\"><button type=\"submit\" id=\"ajax_add_user\"name=\"add_user\" value=\"true\">Отправить</button></td></tr>";
			$str .= "</table></form>";
			
			return $str;
		}
		
		public function mainDisplay() {
			if($this->user_id) { 
				if($this->user_type === 0) {

					echo $this->main;
			
				}
			} else {
				$string="Не зарегистрированный пользователь";
				$this->main = $string;
				echo $this->main;
			}
		}


	}
?>	
