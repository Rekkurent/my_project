<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<?php 
			$this->titleDisplay(); 
			$this->styleDisplay(); 
			$this->jsDisplay();
		?>
	</head>
	<body>
		<div class="header">
			<h1>The Notebook</h1>
		</div>
		<?php 
			$this->menuDisplay($this->menu, "horizontal_menu", $this->current_page); 
		?>
		<div class="main">	
			<div class="content">
			<?php 
				$this->mainDisplayAuth(); 
			?>
			</div>
			<div class="footer"></div>
		</div>
	</body>
</html>

