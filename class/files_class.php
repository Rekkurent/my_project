<?php

	/*
	php.ini
	4 директивы управляющие выгрузкой файлов с помощью PHP
	file_uploads Разрешает или запрещает выгрузку файлов по HTTP. Возможные значения - On (разрешена) Off (запрещена). Значение по умолчанию On
	upload_tmp_dir Задает каталог, где временно храняться выгруженные файлы, ожидающие обработки. Если это значение не заданно используется стандартный системный каталог. Значение по умолчанию NULL
	upload_max_filesize Задает максимально допустимый размер выгружаемых файлов. Если файл больше этого значения, то PHP записывает вместо него пустой файл размером 0 байт. Значение по умолчанию 2M
	post_max_size Задает максимально допустимый размер данных POST, который будет воспринимать PHP, это значение должно быть больше чем upload_max_filesize т.к. оно задает размер любых отправляемых на сервер данных в том числе и выгружаемых файлов. Значение по умолчанию 8M
	*/
	
	
class Files { 
	public $path;
	//диалог по загрузке файлов
	public function uploadingDisplay() {
	//html код для загрузки файлов
		$str ="<div id=\"input\">".
			"<input type=\"file\" multiple id=\"files_ajax\" name=\"f1_multiple\">".
			"<button id=\"files_send\">Отправить</button>".
			"</div>".
			"<div id =\"output\">".
			"<div id=\"client\"></div>".
			"<div id=\"server\"></div>".
			"</div>";
		return $str;
	}
	

	
	public function makeDir($path){
		mkdir($path, 0777);
	}
	public function removeDir($path){
		//удаляемый каталог должен быть пуст
		$files = scandir($path);

		foreach($files as $file) {
			if ($file != "." && $file != "..") {
				$this->deleteFile($path."/".$file);
			}
		}
		
		rmdir($path);
	}
	public function deleteFile($name){
		$name = $name;
		unlink($name);
	}
	
	public function filesInfo() {
		$files = scandir($this->path);
		$str ="<p>Каталог: </p><ul>";
		foreach($files as $file) {
			if ($file != "." && $file != "..") {
				$str.="<li>".$this->fileInfo($this->path, $file)."</li>";
			}
		}
		$str .="</ul>";
		return $str;
	}
	
function file_force_download($file) {
	
	if (file_exists($file)) {
		// сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
		// если этого не сделать файл будет читаться в память полностью!
		if (ob_get_level()) {
			ob_end_clean();
		}
		// заставляем браузер показать окно сохранения файла
		header('Content-Description: File Transfer');
		header('Content-Type: '.filetype($file));
		header('Content-Disposition: attachment; filename=' . basename($file));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($file));
		// читаем файл и отправляем его пользователю
		readfile($file);
		exit;
	}
}
	
	public function fileInfo($path, $name) {
		$full_name = $path."/".$name;
		$str = "<h3>Информация о файле: ".$name."</h3>";
		$str .= "Последнее обращение: ".date('j F Y H:i', fileatime($full_name))."<br />";
		$str .= "Последняя модификация: ".date('j F Y H:i', filemtime($full_name))."<br />";
		/*
		$user = posix_getpwuid(fileowner($full_name));
		$str .= "Владелец файла: ".$user["name"]."<br />";
		$group = posix_getgrgid(filegroup($full_name));
		$str .= "Группа файла: ".$group["name"]."<br />";
		*/
		$str .= "Права доступа: ".decoct(fileperms($full_name))."<br />";
		$str .= "Тип файла: ".filetype($full_name)."<br />";
		$str .= "Размер файла: ".filesize($full_name)." байтов<br />";
		$str .= "<h4>Флаги файла</h4>";
		$str .= "Каталог: ".(is_dir($full_name)? "да" : "нет")."<br />";
		$str .= "Исполняемый: ".(is_executable($full_name)? "да" : "нет")."<br />";
		$str .= "Файл: ".(is_file($full_name)? "да" : "нет")."<br />";
		$str .= "Ссылка: ".(is_link($full_name)? "да" : "нет")."<br />";
		$str .= "Разрешено чтение: ".(is_readable($full_name)? "да" : "нет")."<br />";
		$str .= "Разрешена запись: ".(is_writable($full_name)? "да" : "нет")."<br />";
		return $str;
	}
	
	
	
	//$str .=  dirname($dir->path."/".$file);
	//$str .=  basename($dir->path."/".$file);
	
	public function showDir($name){
		$dir = dir($name);
		//$str = "Свободное место: ".disk_free_space("/")."<br>";
		//$str .="Всего места: ".disk_total_space("/")."<br>";
		//$str = "<div id=\"current_dir\"><p>Каталог: </p><ul>";
		$str="";
		while(false !==($file = $dir->read())){
			if ($file != "." && $file != "..") {
				
				
				$full_name = str_replace($_SERVER["DOCUMENT_ROOT"],"",$dir->path);
				$full_name = ltrim($full_name,"/");
				$str .=  "<li class=\"file\"><a href=\""."get_file.php?name=".urlencode($file)."\">$file</a>";
				$str .= 	"<span class=\"menu\">".
							"<img src=\"icons/delete_note_0.png\" class=\"delete_file\">".
							"</span></li>";
			}
		}
		//$str .= "</ul></div>";
		$dir->close();
		return $str;
	}
	//обработка загруженных файлов
	public function uploadedHandler() {
		
		$files_count = count($_FILES);

		$str ="";
		if($files_count >0) {
			//echo "Количество файлов: ".$files_count."<br>\n";
			
			foreach($_FILES  as $key_elem =>$value_elem) { 
				//echo "Файл ".$key_elem."<br>\n"; // имя файла формы
				if ($value_elem["error"] != UPLOAD_ERR_OK) {
					$str .="Файл ".$value_elem["name"].". Проблема: ";
					switch($value_elem["error"]) {
						case UPLOAD_ERR_INI_SIZE: 
							$str .= "Размер файла больше upload_max_filesize";
							break;
						case UPLOAD_ERR_FROM_SIZE: 
							$str .= "Размер файла больше max_file_size";
							break;
						case UPLOAD_ERR_PARTIAL: 
							$str .= "Загружена только часть файла";
							break;
						case UPLOAD_ERR_NO_FILE: 
							$str .= "Файл не загружен";
							break;
						case UPLOAD_ERR_NO_TMP_DIR: 
							$str .= "Загрузка невозможна: не задан временный каталог";
							break;
						case UPLOAD_ERR_CANT_WRITE: 
							$str .= "Загрузка не выполненна: невозможна запись на диск";
							break;
					}
					echo $str;
				} else {
					$path = $this->path."/".$value_elem["name"];
					$str .= $path."<br>\n";
					if (is_uploaded_file($value_elem["tmp_name"])){
						if(!move_uploaded_file($value_elem["tmp_name"], $path)){
							$str .="Проблема: невозможно переместить файл в каталог назначения";
							echo $str;
						}
					} else {
						$str .="Файл ".$value_elem["name"].".<br>\n";
						$str .= "Проблема: возможна атака через загрузку файла";
						echo $str;
					}
					
					
					
				}
				/*
				$value_elem["name"];
				$value_elem["type"];
				$value_elem["tmp_name"];
				$value_elem["error"];
				$value_elem["size"];
				*/
			}
			
		}
		return $str;
	}
	
}

?>