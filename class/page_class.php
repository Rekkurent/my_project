
<?php

	require_once("arrayModule.php");
	require_once("textModule.php");
	
	class Page {
		public $template;
		public $style;
		public $js;
		public $menu;
		public $title;
		public $current_page;
		public $main;
		public $footer;
		public $array_module;
		public $text_module;
		
		public function get_current_page(){
			$current_page = $_SERVER['PHP_SELF']; 
			$current_page_array = explode( '/', $current_page);
			$current_page = $current_page_array[count($current_page_array)-1];
			return $current_page;
		}
		
		public function __set($name, $value) {
				$this->name = $value;
		}
		
		public function __construct() {
			$this->array_module = new arrayModule();
			$this->text_module = new textModule();
			$this->current_page = $this->get_current_page();
			$this->js = [];

		}
			
		public function redirect() {
			$host  = $_SERVER["HTTP_HOST"];
			$uri   = rtrim(dirname($_SERVER["PHP_SELF"]), '/\\');
			$extra = "index.php";
			header("Location: http://$host$uri/$extra");
			exit;
		}	
			
		public function styleDisplay() {
			$type = gettype($this->style);
			switch($type) {
				case "string": echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"css/".$this->style."\">";
					break;
				case "array": 
					foreach($this->style as $value) {
						echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"css/".$value."\">";
					}
				break;
			}
		}
		
		public function jsDisplay() {
			$type = gettype($this->js);
			switch($type) {
				case "string": echo "<script type=\"text/javascript\" src=\"js/".$this->js."\"></script>";
					break;
				case "array": 
					foreach($this->js as $value) {
						echo "<script type=\"text/javascript\" src=\"js/".$value."\"></script>";
					}
				break;
			}
		}
		
		public function titleDisplay() {
			echo "<title>".$this->title."</title>";
		}
		
		public function mainDisplay() {
			echo $this->main;
		}
		public function footerDisplay() {
			echo "<div class=\"footer\">".$this->footer."</div>";
		}
		
		public function menuDisplay($menu, $style, $current_element) {
			$menu_str = "";
			$menu_str .= "<div class=\"".$style."\">";
			foreach($menu as $key=>$value ) {
				if($value == $current_element) {
					$menu_str .= "<a>$key</a>";
				} else {
					$menu_str .=  "<a href=\"$value\">$key</a>";
				}
				
			}
			$menu_str .= "</div>";
			echo $menu_str;
			
			
		}
			
		public function Display() {
			if ($this->template) {
				require($this->template);
			}
		}		
	}
?>	
