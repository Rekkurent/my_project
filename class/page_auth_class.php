
<?php

	require_once("page_class.php");
	require_once("db_class.php");
	class PageAuth extends Page {
		public $login;
		public $password;
		public $user_id;
		public $user_type;
		public $db;
		public $menu_vertical;
		
		
		public function mainDisplay() {
			if ($this->user_id) {

				echo $this->main;

			} else {
				$string="Неавторизованный пользователь";
				$this->main .= $string;
				echo $this->main;
			}
		}
		
		public function mainDisplayAuth() {
			if($this->user_id) {


			} else {
				$this->authDisplay();
			}
		}
		
		public function pageSessionEnd() { 
				$this->user_id = false; 
				$this->login = "";
				$this->password =""; 
				$_SESSION = array();
				session_unset();
				session_destroy();
				session_write_close();
				setcookie(session_name(),'',0,'/');
				session_regenerate_id(true);
				
		}
		
		function __construct() {
			Parent::__construct();
			
			@session_start();
			
			$this->db = new database();
			if (isset($_SESSION["user_id"])) {
				$this->user_id = $_SESSION["user_id"];
				$this->user_type = $_SESSION["user_type"];
			} else {
				
				$this->setAuth();	
			}
			
		}
		
		function __destruct() {

			$this->db->closeDatabase();
		}
		
		public function setAuth() {
			if ( isset($_POST["login"])) {
				$this->login = $_POST["login"];
			}

			if ( isset($_POST["password"])) {
				$this->password = $_POST["password"];
			}
			if ($this->login && $this->password) {
				$this->userExist();
			}
			
		}

		public function getUserName() {
			$this->db->connectToDatabase();
			$this->db->query = "select login from users where user_id='".$this->user_id."'";
			$this->db->queryExecution();
			
			if (count($this->db->arr_assoc) == 1) {
				$this->login = $this->db->arr_assoc[0]['login'];
			}
		}
		
		public function userExist() {

		
			$this->db->connectToDatabase();
			$this->db->query = "select user_id, user_type from users where login='".$this->login."' and password='".sha1($this->password)."'";
			$this->db->queryExecution();
			
			if (count($this->db->arr_assoc) == 1) {
				$this->user_id = (Integer)$this->db->arr_assoc[0]['user_id'];
				$this->user_type = (Integer)$this->db->arr_assoc[0]['user_type'];
				
				$expireTime = 60*60*24*365; // 365 days
				session_set_cookie_params($expireTime);
				session_regenerate_id(true);
				
				$_SESSION['user_id'] = $this->user_id;
				$_SESSION['user_type'] = $this->user_type;
			}
		}
		
		public function authDisplay() {
			
			?>	
					<div class="Absolute-Center">
						<form name="auth">
						<h2>Вход</h2>
						<label for="login">Логин</label>
						<input type="text" name="login" id="login">
						<label for="password">Пароль</label>
						<input type="password" name="password" id="password">
						<span id="info"></span><br>
						<input type="submit" value="Отправить" name="send">
						<input type="reset" value="Очистить" name="reset">
						</form>
					</div>	
			<?php	
		}

	}
?>	
