<?php

class arrayModule {
				

	public function descriptionToText($descrArray, $textArray){
		$resultArray;
		foreach ($descrArray as $key =>$value) { 
			if ($textArray[$value]) {
				$resultArray[$key] = $textArray[$value];
			}else {
				$resultArray[$key] = $value;
			}
		}
		return $resultArray;
	}
		
	public function arrayToTable($array, $header) {
		$len = count($header);
		$str="<table>";
		$str .="<tr>";
		for ($i = 0; $i < $len; $i++) {
			$str .= "<td>".$header[$i]."</td>";
		}
		$str .="</tr>";
		
		$len = count($array);
		
		for ($i = 0; $i < $len; $i++) {
			$str .="<tr>";
			foreach( $array[$i] as $key => $value) {
				$str .= "<td>".$array[$i][$key]."</td>";
			}
			$str .= "</tr>";
		}
		$str .= "</table>";
		return $str;
	}
		
	public function arrayToTableTitled($array, $header, $title) {

		$str="<table>";
		if (isset($title)) {
			$str .= "<caption>".$title."</caption>";
		}
		
		$str .="<tr>";
		foreach ($header as $key =>$value) {
			$str .= "<td>".$header[$key]."</td>";
		}
		$str .="</tr>";
		
		$len = count($array);
		
		for ($i = 0; $i < $len; $i++) {
			$str .="<tr>";
			foreach( $array[$i] as $key => $value) {
				$str .= "<td>".$array[$i][$key]."</td>";
			}
			$str .= "</tr>";
		}
		$str .= "</table>";
		return $str;
	}
		
	public function normalizeArray(&$array, $arrayFunc, &$obj) {
		
		$len = count($array);
		for($i = 0; $i < $len; $i++ ){
			foreach($array[$i] as $key => $value){
				if(isset($arrayFunc[$key])) {
					$array[$i][$key] = $obj->$arrayFunc[$key]($value);
				}
			}
		}
	}
		
				
	public function copyArray(&$arr1, $arr2,&$result) {
		
		$len = count($arr1);
		$type = gettype($arr1[0]);
		switch($type) {
			case "array" : {
				foreach($arr2 as $value){
					for($i = 0; $i <$len; $i++) {
						if(isset($arr1[$i][$value])){

							if(gettype($arr2) =="string" || count($arr2) ==1) {
								$result[$i]= $arr1[$i][$value];
							} else {
								$result[$i][$value] = $arr1[$i][$value];
							}
						}
							
					}
				}
			}
			break;
			default : {
				
				foreach($arr2 as $value){	
						$result[$value] = $arr1[$value];
				}
				
			}
			break;
		}
	}
		
	public function cutArray(&$arr1, $arr2, &$result) {
		
		$len = count($arr1);
		$type = gettype($arr1[0]);
		switch($type) {
			case "array" : {
				foreach($arr2 as $value){
					for($i = 0; $i <$len; $i++) {
						if(isset($arr1[$i][$value])){

							if(gettype($arr2) =="string" || count($arr2) ==1) {
								$result[$i]= $arr1[$i][$value];
							} else {
								$result[$i][$value] = $arr1[$i][$value];
							}
						}
						
						unset ($arr1[$i][$value]);
					}
				}
			}
			break;
			default : {
				if (gettype($arr2) == "array") {
					foreach($arr2 as $value) {	
						for($i = 0; $i <$len; $i++) {
							if ($arr1[$i] ==$value) {
								$result[$i] = $arr1[$i];
								unset ($arr1[$i]);
							}
						}
					}
				}
				else {
					for($i = 0; $i <$len; $i++) {
						if ($arr1[$i] ==$arr2) {
							$result[$i] = $arr1[$i];
							unset ($arr1[$i]);
						}
					}
				}

			}
			break;
		}
	}
		
	public function catArray2(&$arr1, $arr2) {
		
		$len = count($arr1);
		for($i=0; $i < $len; $i++) {
				$arr1[$i][count($arr1[$i])] = $arr2[$i];
		}
	}
					
	public function catArray(&$arr1, $arr2) {
		
		$len = count($arr1);
		$type = gettype($arr1[0]);
		switch($type) {
			case "array" : {
				foreach($arr2 as $key => $value){
					for($i = 0; $i <$len; $i++) {
						$typekey = gettype($key);

						if($typekey == "integer") {
							$len2=count($arr1[$i]);
							$arr1[$i][$len2] = $value;
						} else {
							$arr1[$i][$key] = $value;
						}
					}
				}
			}
			break;
			default : {

				foreach($arr2 as $key => $value){	
					$typekey = gettype($key);
					if($typekey == "integer") {
						$len2=count($arr1);
						$arr1[$len2] = $value;
					} else {
						$arr1[$key] = $value;
					}
				}
				
			}
			break;
		}

	}
}
		
?>