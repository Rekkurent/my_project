<?php

require_once("arrayModule.php");
require_once("textModule.php");
require_once("url_class.php");
	
class Notes {
	public $db;
	public $user_id;
	public $array_module;
	public $text_module;
	public $url_analyze;
	
	public function __construct(&$db, $user_id){
		$this->db = $db;
		$this->user_id = $user_id;
		$this->url_analyze = new Url();
		
	}
	
	/*
	public function objDisplay(&$obj){
		$tmp = explode("\r\n", $obj);
		$length = count($tmp);
		$str="";
		for ($i = 0; $i < $length; $i++){
			$str.="<div class=\"str\" id=\"str_".$i."\">".$tmp[$i]."</div>";
		}
		return $str;

		
	}
	*/
	
	public function notesDisplay(&$string){
		$array =[];
		$this->notesArray($array);
		$length = count($array);		
		$str ="";

		$str .= "<div class=\"notes\">";	
		$tmp="";		
		for($i =0; $i < $length; $i++) {
			
			$array[$i]["title"] = htmlspecialchars($array[$i]["title"]);
			//$array[$i]["content"] = htmlspecialchars($array[$i]["content"]);

			$str.="<div id=\"".$array[$i]["note_id"]."\" class=\"note\">".
			"<div class=\"title_note\"><h2 class=\"title\">".$array[$i]["title"]."</h2>".
			"<div class=\"menu\">".
			"<img src=\"icons/edit_note_0.png\" class=\"edit_note\">".
			"<img src=\"icons/delete_note_0.png\" class=\"delete_note\">".
			"</div>".
			"</div>".
			"<div class=\"content_note\">".
			"<div class=\"body_note\">".
			$array[$i]["content"].
			"</div>".
			"</div>".
			"</div>";
		}
		$str.="</div>";
		$string = $str;
		
	}
	public function notesArray(&$array){
		
			
		$this->db->connectToDatabase();
		$this->db->query = "select note_id, title, content from notes where user_id=".$this->user_id;
		$this->db->queryExecution();

		$arr = $this->db->arr_assoc;
		
		if (!get_magic_quotes_gpc()) {
			$length = count($arr);	
			for($i =0; $i < $length; $i++) {		
				$arr[$i]["title"] = stripslashes($arr[$i]["title"]);
				//$arr[$i]["content"] = stripslashes($arr[$i]["content"]);
			} 
		}
		$array = $arr;


	}
	
	public function addNoteDisplay(){
		$str="<form id=\"input_form\">".
			"<div class=\"header_add_note\"><h2>Добавить заметку</h2></div>".
			"<div class=\"content_add_note\">".
				"<label for=\"title\">Заголовок:</label><br><input type=\"text\" id=\"title\" name=\"title\"><br>".
				"<label for=\"note\">Заметка:</label><br><textarea id=\"note\" name=\"note\"></textarea><br>".
				"<button type=\"submit\" name=\"ajax_add\" id=\"add_button\">Добавить</button>".
				"<button type=\"submit\" name=\"ajax_del_all\" id=\"del_all_button\">Удалить все заметки</button>".
			"</div>".
		"</form>";
		return $str;
	}
	
	public function delAllNotes(){
		$this->db->connectToDatabase();
		$this->db->query ="delete from notes where user_id=".$this->user_id;
		$this->db->queryExecution();
	}
	
	public function delNote($note_id){
		$this->db->connectToDatabase();
		$this->db->query ="delete from notes where user_id=".$this->user_id." and note_id=".$note_id;
		$this->db->queryExecution();
	}	


	public function addNote(&$title, &$content){
		
		
		$content = $this->url_analyze->Show($content);
		
		if (!get_magic_quotes_gpc()) {
			$db_title = addslashes($title); 
			$db_content = addslashes($content); 
		}
		$this->db->connectToDatabase();

		$this->db->query ="insert into notes set user_id=".$this->user_id.", title=\"".$db_title."\", content=\"".$db_content."\"";
		$this->db->queryExecution();
		
		$this->db->query ="select MAX(note_id) as note_id from notes where user_id=".$this->user_id;
		$this->db->queryExecution();
		
		return $this->db->arr_assoc[0]["note_id"];
	
		
	}
	
	public function updateNote($id, &$title, &$content) {
		
		
		
		if (!get_magic_quotes_gpc()) {
			$db_title = addslashes($title); 
			$db_content = addslashes($content); 
		}
		
		$this->db->connectToDatabase();
		
		$this->db->query ="update notes set title=\"".$db_title."\", content=\"".$db_content."\" where user_id=".$this->user_id." and note_id=".$id;
		$this->db->queryExecution();
		
	}
	
	
	public function addNotes($array){
		$arr = [];
		foreach($array as $value) {
			$arr[] = $this->addNote($value["title"], $value["content"]);
		}
		return $arr;
	}
}
?>