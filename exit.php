<?php

	require_once("class/page_auth_class.php");
	
	$page = new PageAuth();
	
	if ($page->user_id) {
		
		$page->pageSessionEnd();

	}	
	
	$page->redirect();

?>