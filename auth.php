<?php

	require_once("class/page_auth_class.php");
	require_once("configure/configure.php");
	
	$page = new PageAuth();

	if ($page->user_id) {
		
		$page->redirect();
		
	} else {

		$type = "auth";
		configure($page, $type);
		
		$page->title = "Вход";
		$page->Display();	
		
	}
	
?>