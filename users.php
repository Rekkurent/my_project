<?php

	require_once("class/page_admin_class.php");
	require_once("configure/configure.php");
	
	$page = new PageAdmin();

	if ($page->user_type === 0 ) {
		
		$type = "admin";
		configure($page, $type);
		
		$page->title = "Пользователи";
		
		array_push($page->js, "admin.js");
		
		$page->main .= $page->usersListDisplay();
		$page->main .= $page->printAddUser();
		$page->Display();	
	
	} else {
		
		$page->redirect();
		
	}
	
	
?>